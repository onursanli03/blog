### Laravel Blog v1.0

 This is my first blog project on laravel.

### Easy Setup
* clone repository
* go to project folder on command window
* use this composer command (you have not [composer ](http://getcomposer.org/download) download and install it.)
   
```
#!bash

composer install
```

* create a schema on your database (mysql,pl-sql,t-sql unnecessarily)
* generate a new laravel application key with this command
   
```
#!bash

php artisan key:generate
```

* and doing migrations
  
```
#!bash

php artisan migrate
```


### License

This project and the underlying Laravel framework are open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)